//
//  MainScreenController.swift
//  reusableView
//
//  Created by Dmitry Uskov on 02.04.15.
//  Copyright (c) 2015 Dmitry Uskov. All rights reserved.
//

import UIKit

class MainScreenController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // растягиваем scrollView
        let screenSize: CGRect = UIScreen.mainScreen().bounds
        scrollView.frame = screenSize
        
        var list = [0,1,2,3,4,5,6]        
        
        for index in list {
            
            var card: EventCardView  = EventCardView();
            card.view.frame = CGRect(x: 0, y: (CGFloat(index) * scrollView.bounds.height)/2, width: scrollView.bounds.width, height: scrollView.bounds.height / CGFloat (2))
            
//            card.shadowView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.1)
            
//            card.shadowView.layer.borderColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.2).CGColor
//            card.shadowView.layer.borderWidth = CGFloat(1)
            
            var layer = card.shadowView.layer
            
            layer.shadowColor = UIColor.blackColor().CGColor
            layer.shadowOffset = CGSize(width: 0, height: 0)
            layer.shadowOpacity = 0.4
            layer.shadowRadius = 5
            
            
//            card.image.image = UIImage()
            card.eventLabel.text = "Card #\(index)"
            println("Card #\(index) added")
            
            scrollView.addSubview(card)
        }
        scrollView.contentSize = CGSize(width: scrollView.bounds.width, height: scrollView.bounds.height * CGFloat(list.count) / CGFloat (2))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
