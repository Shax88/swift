//
//  EventCardView.swift
//  reusableView
//
//  Created by Dmitry Uskov on 02.04.15.
//  Copyright (c) 2015 Dmitry Uskov. All rights reserved.
//

import UIKit

class EventCardView: UIView {

    @IBOutlet var view: UIView!
    
    @IBOutlet weak var shadowView: UIView!
    
    @IBOutlet weak var eventLabel: UILabel!
    
    required init(coder aDecoder: NSCoder) {
        fatalError("Not support NSCoding!")
//        super.init(coder: aDecoder)
//        //bundle
//        NSBundle.mainBundle().loadNibNamed("EventCardView", owner: self, options: nil)
//        
//        self.addSubview(self.card)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame);
        //bundle
        NSBundle.mainBundle().loadNibNamed("EventCardView", owner: self, options: nil)
        
        self.addSubview(view)
    }
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
