//
//  ArtistTableViewCell.swift
//  cwm-spring
//
//  Created by Dmitry Uskov on 09.04.15.
//  Copyright (c) 2015 Dmitry Uskov. All rights reserved.
//

import UIKit
import Spring

class ArtistTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var subtitleLabel: UILabel!
    
    @IBOutlet weak var photoImageView: AsyncImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        self.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
