//
//  ViewController.swift
//  cwm
//
//  Created by Dmitry Uskov on 02.04.15.
//  Copyright (c) 2015 Dmitry Uskov. All rights reserved.
//

import UIKit
import Spring

class ViewController: UIViewController {

    
    @IBAction func click(sender: DesignableButton) {
        sender.animation = "flipY"
        sender.curve = "spring"
        sender.duration = 1.0
        //        sender.backgroundColor = UIColor.redColor()
        
        sender.animate()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // растягиваем scrollView
//        let screenSize: CGRect = UIScreen.mainScreen().bounds
//        scrollView.frame = screenSize
//        var card  = EventCardView();
//        card.view.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height)
//        self.view.addSubview(card)

        
    }
    
    func pressed(sender: DesignableButton) {
//        println("Pressed!")
//        sender.animation = "flipX"
//        sender.curve = "spring"
//        sender.duration = 1
//        sender.animate()
        var alertView = UIAlertView();
        alertView.addButtonWithTitle("Ok");
        alertView.title = "title";
        alertView.message = "message";
        alertView.show();
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initTests() {
        var list: [Int] = [0,1,2,3,4,5,6]
        
        var images = [
            "https://i.concertwith.me/artists/51e8cf0c62b1e30e240dfab750241b1f.jpg?size=medium",
            "https://i.concertwith.me/artists/de3f23eacf12821db53102b3707f1d37.jpg?size=medium",
            "https://i.concertwith.me/artists/2772e3fd66461b2aa2f45c965c822189.jpg?size=medium",
            "https://i.concertwith.me/artists/56f76e153d6c32126d7cef5452dfad1b.jpg?size=medium",
            "https://i.concertwith.me/artists/51b9a8eea1e4e8e569bea9012fbb3565.jpg?size=medium",
            "https://i.concertwith.me/artists/8eddada8f68bcc0deadcb4d196ddb344.jpg?size=medium",
            "https://i.concertwith.me/artists/3e26f2c9ec5862afad2cb750149fdb46.jpg?size=medium",
            "https://i.concertwith.me/artists/b5629861b1d125beaa7288a4e9a5ad7b.jpg?size=medium"
        ]
        
        for index in list {
            
            var card: EventCardView  = EventCardView();
//            card.view.frame = CGRect(x: 0, y: (CGFloat(index) * scrollView.bounds.height)/2, width: scrollView.bounds.width, height: scrollView.bounds.height / CGFloat (2))
            
            // Лепим картинку
            //            card.image.setURL(NSURL(string: images[index]), placeholderImage: UIImage(named: "defaultImage"))
            //            card.image.setURL()
            //
            //            //            card.shadowView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.1)
            //
            //            //            card.shadowView.layer.borderColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.2).CGColor
            //            //            card.shadowView.layer.borderWidth = CGFloat(1)
            //
            //
            //
            //
            //            //            card.image.image = UIImage()
            //            card.titleLabel.text = "Card #\(index)"
            //            println("Card #\(index) added")
            
            println(card.userInteractionEnabled)
//            scrollView.addSubview(card)
        }
        // Ставим ширину высота карточек * 2
//        scrollView.contentSize = CGSize(width: scrollView.bounds.width, height: scrollView.bounds.height * CGFloat(list.count) / CGFloat (2))
    }

    func initCards() {
        let url = "http://api.concertwith.me/main?type=all&festival=all&place=57&month=all"
        
        var request = NSURLRequest(URL: NSURL(string: url)!)
        var data = NSURLConnection.sendSynchronousRequest(request, returningResponse: nil, error: nil)
        if data != nil {
            // Парсим json
            var json = JSON(data: data!)
            
            // Количество событий, которое пришло
            var countEvents: Int = 0
            
            let events = json["data"].arrayValue
            
            //Форматтер дат
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateStyle = NSDateFormatterStyle.MediumStyle
            
            for (index, eventJson) in enumerate(events) {
                println(index)
                println(eventJson["cover"])
                
                // Если это не промо страничка
                if(!eventJson["promo"].boolValue) {
                    var card: EventCardView  = EventCardView();
//                    card.view.frame = CGRect(x: 0, y: (CGFloat(index) * scrollView.bounds.height)/2, width: scrollView.bounds.width, height: scrollView.bounds.height / CGFloat (2))
                    
                    // Лепим картинку
                    card.image.setURL(NSURL(string: eventJson["cover"].string!), placeholderImage: UIImage(named: "defaultImage"))
                    
                    // Заголовок события
                    card.titleLabel.text = eventJson["title"].string
                    
                    // Дата
                    card.dateLabel.text = dateFormatter.stringFromDate(NSDate(timeIntervalSince1970: Double(eventJson["date_start"].double!)))
                    
                    // Название площадки
                    card.venueLabel.text = eventJson["venue"].string
                    
                    card.wtcButton.addTarget(self, action: "pressed:", forControlEvents: .TouchUpInside)
                    
                    //            card.image.setURL()
                    //
                    //            //            card.shadowView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.1)
                    //
                    //            //            card.shadowView.layer.borderColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.2).CGColor
                    //            //            card.shadowView.layer.borderWidth = CGFloat(1)
                    //
                    //
                    //
                    //
                    //            //            card.image.image = UIImage()
                    //            card.titleLabel.text = "Card #\(index)"
                    //            println("Card #\(index) added")
                    
                    
//                    scrollView.addSubview(card)
                    countEvents++
                }
            }
            println(countEvents)
            
            // Ставим ширину высота карточек * 2
//            scrollView.contentSize = CGSize(width: scrollView.bounds.width, height: scrollView.bounds.height * CGFloat(countEvents) / CGFloat (2))
        }

    }

}

