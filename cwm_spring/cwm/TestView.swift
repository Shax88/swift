//
//  TestView.swift
//  cwm-spring
//
//  Created by Dmitry Uskov on 07.04.15.
//  Copyright (c) 2015 Dmitry Uskov. All rights reserved.
//

import UIKit

@IBDesignable class TestView: UIView {

    
    @IBOutlet var mainVIew: UIView!
    
    @IBOutlet weak var buttonNew: UIButton!
    
    @IBAction func bPressed(sender: AnyObject) {
        pressed()
    }
    
    func pressed() {
        println("Pressed")
        var alertView = UIAlertView();
        alertView.addButtonWithTitle("Ok");
        alertView.title = "title";
        alertView.message = "message";
        alertView.show();
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame);
        
        xibSetup()
    }

    required init(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
        super.init(coder: aDecoder)
        
        xibSetup()
    }
    
//    func linkXib() {
//        //bundle
//        NSBundle.mainBundle().loadNibNamed("TestView", owner: self, options: nil)
//        self.userInteractionEnabled = true
//        
//        self.addSubview(mainView)
//    }

    func xibSetup() {
        mainVIew = loadViewFromNib()
        
        // use bounds not frame or it'll be offset
        mainVIew.frame = bounds
        
        // Make the view stretch with containing view
        mainVIew.autoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight
        
//       buttonNew.addTarget(self, action: "pressed:", forControlEvents: .TouchUpInside)
        
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(mainVIew)
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "TestView", bundle: bundle)
        
        // Assumes UIView is top level and only object in CustomView.xib file
        let mainView = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        return mainView
    }

}
