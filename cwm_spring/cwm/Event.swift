//
//  Event.swift
//  cwm-spring
//
//  Created by Dmitry Uskov on 13.04.15.
//  Copyright (c) 2015 Dmitry Uskov. All rights reserved.
//

import Foundation


class Event {
    
    var id: Int
    var title: String
    var venue: VenueShort
    var dateStart: String
    var image: String
    var isGoing: Bool = false
    var dateFormatted: String
    
    var description: String
    
    var artists: [ArtistShort] = []
    var tickets: [TicketShort] = []
    var headlinerName: String
    
    var lat: Double
    var long: Double
    
    init( eventJson: JSON) {
        id = eventJson["id"].intValue
        // Лепим картинку
        var img = eventJson["cover"].string
        
        headlinerName = eventJson["headliner"].string!
        
        if (eventJson["prepared_cover"].string != nil) {
            img  = "https://i.concertwith.me/" + eventJson["prepared_cover"].string!
        }
        
        image = img!
        
        title = eventJson["title"].string!
        
        // Дата
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateStyle = NSDateFormatterStyle.MediumStyle
        dateStart = dateFormatter.stringFromDate(NSDate(timeIntervalSince1970: Double(eventJson["date_start"].double!)))
        
        // Площадка
        venue = VenueShort(eventJson: eventJson)
        
        dateFormatted = eventJson["date_formatted_mobile"].string!
        
        
        isGoing = eventJson["wtc"].boolValue
        
        
        // Запихиваем артистов
        for artistJson in eventJson["artists"].arrayValue {
            artists.append(ArtistShort(artistJson: artistJson))
        }
        
        // Запихиваем билеты
        for ticketJson in eventJson["tickets"].arrayValue {
            tickets.append(TicketShort(ticketJson: ticketJson))
        }
        
        description = StringHelper.cleanStr(eventJson["description"].stringValue)
        
//        println(eventJson["loc"]["lat"].doubleValue)
        // Координаты
        lat = eventJson["loc"]["lat"].doubleValue
        long = eventJson["loc"]["lng"].doubleValue
    }
    
    
    
}