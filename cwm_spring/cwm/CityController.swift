//
//  CityController.swift
//  cwm
//
//  Created by Dmitry Uskov on 02.04.15.
//  Copyright (c) 2015 Dmitry Uskov. All rights reserved.
//

import UIKit
import Spring

class CityController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet weak var collection: UICollectionView!
    
    var events: [CityEvent] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collection.delegate = self
        collection.dataSource = self
        
        // Ставим правильные размеры для ячеек и самой коллекции (изначально он не умеет задавать правильные размеры)
        var layout: UICollectionViewFlowLayout  =  collection.collectionViewLayout as! UICollectionViewFlowLayout;
        collection.bounds.size = view.bounds.size
        layout.itemSize = CGSize(width: view.bounds.width, height: view.bounds.height / 2)
        
        let url = "http://api.concertwith.me/main?type=all&festival=all&place=18&month=all"
        
        var request = NSURLRequest(URL: NSURL(string: url)!)
        
        // Качаем данные
        var data = NSURLConnection.sendSynchronousRequest(request, returningResponse: nil, error: nil)
        if data != nil {
            // Парсим json
            var json = JSON(data: data!)
            
            // Количество событий, которое пришло
            var countEvents: Int = 0
            
            var tmpEvents = json["data"].arrayValue
            
            for (index, eventJson) in enumerate(tmpEvents) {
                
                // Если это не промо страничка
                if(!eventJson["promo"].boolValue) {
                    events.append(CityEvent(eventJson: eventJson))
                }
            }
            println(events.count)
            collection.reloadData()
        }
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell: EventViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("EventViewCell", forIndexPath: indexPath) as! EventViewCell
        
//
        let event = events[indexPath.row]
        
            
        let card = cell.eventCard
        
        // Делаем карточки правильного размера
        let size = CGSize(width: cell.bounds.width - 16, height: cell.bounds.height - 16)
        card.view.frame.size = size
        
        
        // Лепим картинку
        var img = event.image
        
        card.image.setURL(NSURL(string: img), placeholderImage: UIImage(named: "defaultImage"))
        
        // Заголовок события
        card.titleLabel.text = event.title
        
        // Дата
        
        card.dateLabel.text = event.dateStart
        // Название площадки
        card.venueLabel.text = event.venue
        
        var button = card.wtcButton
        // Запоминаем в какой ячейке лежит кнопка
        button.tag = indexPath.row
        // Устанавливаем состояние
        setWtcButtonState(button, isGoing: event.isGoing)
        
        card.wtcButton.addTarget(self, action: "pressed:", forControlEvents: UIControlEvents.TouchUpInside)
        
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return events.count
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
//        var alertView = UIAlertView();
//        alertView.addButtonWithTitle("Ok");
//        alertView.title = "title";
//        alertView.message = "# \(indexPath.row) selected";
//        alertView.show();
        var cell: EventViewCell = collectionView.cellForItemAtIndexPath(indexPath) as! EventViewCell
        let card = cell.eventCard
        
        let main = self.view as! DesignableView
//        if (main.animation == "") {
            main.animation = "slideLeft"
            main.force = 4
            main.curve = "spring"
            main.duration = 1
            main.animateToNext(nextMy)
//        } else {
//            main.animateNext(nextMy)
//        }
        println("animated!")
        
//        
//        main.animation = "slideLeft"
//        main.force = 4
//        main.curve = "spring"
//        main.duration = 0.8
//        main.animate()
        
        
//        card.animation = "flipY"
//        card.curve = "spring"
//        card.duration = 2
//        card.animateNext(nextMy)
        
//        performSegueWithIdentifier("toEvent", sender: self)
        
    }
    
    func nextMy() {
        performSegueWithIdentifier("toEvent", sender: self)
    }
    
    
    func collectionView(collectionView: UICollectionView, willDisplayCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
//        var cell: EventViewCell = collectionView.cellForItemAtIndexPath(indexPath) as! EventViewCell
        var uiCell: EventViewCell = cell as! EventViewCell
        let card = uiCell.eventCard
        
        card.animation = "slideRight"
        card.curve = "spring"
        card.duration = 0.3
        card.scaleX = 10.0
        card.scaleY = 10.0
        card.damping = 1.0
        card.animate()
    }
    
//    func collectionView(collectionView: UICollectionView, didEndDisplayingCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
//        var cell: EventViewCell = cell as! EventViewCell
//        
//        let card = cell.eventCard
//        println(card.titleLabel.text)
//    }
//    
//    func collectionView(collectionView: UICollectionView, didUnhighlightItemAtIndexPath indexPath: NSIndexPath) {
//                var cell: EventViewCell = collectionView.cellForItemAtIndexPath(indexPath) as! EventViewCell
//                let card = cell.eventCard
////        println(card.titleLabel.text)
//    }
    
    
    func collectionView(collectionView: UICollectionView, didEndDisplayingSupplementaryView view: UICollectionReusableView, forElementOfKind elementKind: String, atIndexPath indexPath: NSIndexPath) {
        var q: EventViewCell = view as! EventViewCell
        println(q.eventCard.wtcButton.tag)
//        var cell: EventViewCell = collectionView.cellForItemAtIndexPath(indexPath) as! EventViewCell
//        let card = cell.eventCard
//        
//        card.animation = "slideRight"
//        card.curve = "spring"
//        card.duration = 1.0
//        card.animateTo()

    }
    
    func setWtcButtonState(button: DesignableButton, isGoing: Bool) {
        if (!isGoing) {
            button.backgroundColor = UIColor(hex: "#2F71B8")
            button.setTitle("Go!", forState: UIControlState.Normal)
        } else {
            button.backgroundColor = UIColor(hex: "#32AD42")
            button.setTitle("Going", forState: UIControlState.Normal)
        }
    }
   
    
    func pressed(sender: DesignableButton!) {
        let oldTitle = sender.titleLabel?.text
        sender.setTitle("", forState: UIControlState.Normal)
        sender.animation = "morph"
        sender.curve = "linear"
        sender.duration = 1.0
        sender.animateTo()
        
        let event = getCityEventModel(sender.tag)
        event.isGoing = !event.isGoing
        setWtcButtonState(sender, isGoing: event.isGoing)
       
    }
    
    func getCityEventModel(index: Int) -> CityEvent {
        return events[index]
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        
        if UIDevice.currentDevice().orientation.isLandscape.boolValue {
            // Ставим правильные размеры для ячеек и самой коллекции (изначально он не умеет задавать правильные размеры)
            var layout: UICollectionViewFlowLayout  =  collection.collectionViewLayout as! UICollectionViewFlowLayout;
            collection.bounds.size = view.bounds.size
            layout.itemSize = CGSize(width: size.width, height: size.height)
        } else {
            // Ставим правильные размеры для ячеек и самой коллекции (изначально он не умеет задавать правильные размеры)
            var layout: UICollectionViewFlowLayout  =  collection.collectionViewLayout as! UICollectionViewFlowLayout;
            collection.bounds.size = view.bounds.size
            layout.itemSize = CGSize(width: size.width, height: size.height / 2)
//            collection.reloadData()
        }
        println("changed")
        collection.reloadData()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        super.prepareForSegue(segue, sender: sender)
        if (segue.identifier == "toEvent") {
            let eventController: EventViewController = segue.destinationViewController as! EventViewController
            let event = getCityEventModel(collection!.indexPathsForSelectedItems().first!.row)
            eventController.currentEventId = event.id
        }
    }

}
