//
//  VenueShort.swift
//  cwm-spring
//
//  Created by Dmitry Uskov on 13.04.15.
//  Copyright (c) 2015 Dmitry Uskov. All rights reserved.
//

import Foundation

class VenueShort {
    
    var name: String
    var url: String
    var country: String
    var countryCode: String
    var city: String
    var region: String
    var street: String
    
    
    init (eventJson: JSON) {
        name = eventJson["venue_name"].string!
        url = eventJson["venue_url"].stringValue
        country = eventJson["country"].string!
        countryCode = eventJson["country_code"].string!
        city = eventJson["city"].string!
        region = eventJson["region"].stringValue
        street = eventJson["street"].string!
    }
}