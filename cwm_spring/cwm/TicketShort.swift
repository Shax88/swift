//
//  TicketShort.swift
//  cwm-spring
//
//  Created by Dmitry Uskov on 13.04.15.
//  Copyright (c) 2015 Dmitry Uskov. All rights reserved.
//

import Foundation


class TicketShort {   
    
    
    var serviceId: Int
    var deleteFlag: Bool
    var link: String
    var char: String
    var formatted: String
    var formattedMax: String
    var price: Float
    var priceMax: Float
    
    
    init( ticketJson: JSON) {
        serviceId = ticketJson["service_id"].intValue
        
        deleteFlag = ticketJson["delete_flag"].boolValue
        link = ticketJson["link"].string!
        char = ticketJson["char"].string!
        formatted = ticketJson["formatted"].string!
        if (ticketJson["formatted_max"].boolValue) {
            formattedMax = ticketJson["formatted_max"].string!
        } else {
            formattedMax = ""
        }
        price = ticketJson["price"].floatValue
        priceMax = ticketJson["price_max"].floatValue
        
        
    }
    
}