//
//  StringHelper.swift
//  cwm-spring
//
//  Created by Dmitry Uskov on 13.04.15.
//  Copyright (c) 2015 Dmitry Uskov. All rights reserved.
//

import Foundation

class StringHelper {
    class func cleanStr(str: String) -> String{
        let regex:NSRegularExpression  = NSRegularExpression(
            pattern: "<.*?>",
            options: NSRegularExpressionOptions.CaseInsensitive,
            error: nil)!
        
        
        let range = NSMakeRange(0, str.length)
        let htmlLessString: String = regex.stringByReplacingMatchesInString(str,
            options: NSMatchingOptions.allZeros,
            range:range ,
            withTemplate: "")
        return htmlLessString
    }
}