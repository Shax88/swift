//
//  Partner.swift
//  cwm-spring
//
//  Created by Dmitry Uskov on 13.04.15.
//  Copyright (c) 2015 Dmitry Uskov. All rights reserved.
//

import Foundation

class Partner {
    
    var id: Int
    var name: String
    var eventsCount: Int
    var like: Bool = false
    var image: String
    
    
    init( artistJson: JSON) {
        id = artistJson["id"].intValue
        // Лепим картинку
        var img = artistJson["pic"].string
        
        if (artistJson["prepared_cover"].string != nil) {
            img  = "https://i.concertwith.me/" + artistJson["prepared_cover"].string!
        }
        
        image = img!
        
        name = artistJson["name"].string!
        
        eventsCount = artistJson["events_count"].intValue
        
        
    }
    
}