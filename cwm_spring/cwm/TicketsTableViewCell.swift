//
//  TicketsTableViewCell.swift
//  cwm-spring
//
//  Created by Dmitry Uskov on 10.04.15.
//  Copyright (c) 2015 Dmitry Uskov. All rights reserved.
//

import UIKit
import Spring

class TicketsTableViewCell: UITableViewCell {

    @IBOutlet weak var priceLabel: UILabel!
    
    @IBOutlet weak var partnerLabel: UILabel!
    
    @IBOutlet weak var partnerImageView: AsyncImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
