//
//  CityEvent.swift
//  cwm-spring
//
//  Created by Dmitry Uskov on 08.04.15.
//  Copyright (c) 2015 Dmitry Uskov. All rights reserved.
//

import Foundation

class CityEvent {
    
    var id: Int
    var title: String
    var venue: String
    var dateStart: String
    var image: String
    var isGoing: Bool = false
    
    init( eventJson: JSON) {
        id = eventJson["id"].intValue
        // Лепим картинку
        var img = eventJson["cover"].string
        
        if (eventJson["prepared_cover"].string != nil) {
            img  = "https://i.concertwith.me/" + eventJson["prepared_cover"].string!
        }
        
        image = img!
        
        title = eventJson["title"].string!
        
        // Дата
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateStyle = NSDateFormatterStyle.MediumStyle
        dateStart = dateFormatter.stringFromDate(NSDate(timeIntervalSince1970: Double(eventJson["date_start"].double!)))
        
        // Название площадки
        venue = eventJson["venue"].string!
        
        
    }
    
}