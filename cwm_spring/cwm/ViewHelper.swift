//
//  ViewHelper.swift
//  cwm-spring
//
//  Created by Dmitry Uskov on 10.04.15.
//  Copyright (c) 2015 Dmitry Uskov. All rights reserved.
//

import Foundation
import UIKit

class ViewHelper {
    
    class func normalizeTableCell (cell: UITableViewCell) {
        cell.contentView.backgroundColor = UIColor.clearColor()
        cell.backgroundColor = UIColor(white: 1.0, alpha: 0.0)
        cell.layoutMargins = UIEdgeInsetsZero
        cell.preservesSuperviewLayoutMargins = false;
    }
    
}