//
//  EventDetailView.swift
//  cwm-spring
//
//  Created by Dmitry Uskov on 09.04.15.
//  Copyright (c) 2015 Dmitry Uskov. All rights reserved.
//

import UIKit
import Spring
import MapKit

@IBDesignable class EventDetailView: DesignableView {

    
    @IBOutlet var view: UIView!
    
    @IBOutlet weak var mapVIew: MKMapView!
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        xibSetup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame);
        
        xibSetup()
    }
        
    
    func xibSetup() {
        view = loadViewFromNib()
        
        // use bounds not frame or it'll be offset
        view.frame = bounds
        
        
        // Make the view stretch with containing view
        view.autoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight
       
        addSubview(view)
        
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "EventDetailView", bundle: bundle)
        
        // Assumes UIView is top level and only object in CustomView.xib file
        let viewNew = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        return viewNew
    }

}
