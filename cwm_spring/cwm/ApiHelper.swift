//
//  ApiHelper.swift
//  cwm-spring
//
//  Created by Dmitry Uskov on 06.04.15.
//  Copyright (c) 2015 Dmitry Uskov. All rights reserved.
//

import Foundation

class ApiHelper {
    
    
    class func getJSON(urlToRequest: String) -> NSData{
        return NSData(contentsOfURL: NSURL(string: urlToRequest)!)!
    }   
    
    class func parseJSON(inputData: NSData) -> NSDictionary{
        var error: NSError?
        var boardsDictionary: NSDictionary = NSJSONSerialization.JSONObjectWithData(inputData, options: NSJSONReadingOptions.MutableContainers, error: &error) as! NSDictionary
        
        return boardsDictionary
    }
}