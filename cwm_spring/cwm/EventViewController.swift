//
//  EventViewController.swift
//  cwm-spring
//
//  Created by Dmitry Uskov on 09.04.15.
//  Copyright (c) 2015 Dmitry Uskov. All rights reserved.
//

import UIKit
import MapKit
import Spring

class EventViewController: UIViewController, MKMapViewDelegate , UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var mapView: MKMapView!
    
    @IBOutlet weak var imageView: AsyncImageView!
    
    @IBOutlet weak var artistsTableView: UITableView!
    
    @IBOutlet weak var eventDescTitleLabel: UILabel!
    
    @IBOutlet weak var eventDescLabel: UILabel!
    
    @IBOutlet weak var ticketsTableView: UITableView!
    
    @IBOutlet weak var mapGradientView: UIView!
    
    @IBOutlet weak var venueNameLabel: UILabel!
    
    @IBOutlet weak var venueAdrLabel: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var artistLabel: UILabel!
    
    @IBOutlet weak var artTopLineView: UIView!
    
    @IBOutlet weak var tickTopLineView: UIView!
    
    
    /// Constraints
    
    @IBOutlet weak var artTableHeightConstraint : NSLayoutConstraint!
    
    @IBOutlet weak var ticketsTableHeightConstraint : NSLayoutConstraint!
    
    @IBOutlet weak var contentViewConstraint : NSLayoutConstraint!
    
    @IBOutlet weak var imageViewConstraint : NSLayoutConstraint!
    
    @IBOutlet weak var mapViewConstraint : NSLayoutConstraint!
    
    // Tap recognizers
    let mapTapRecognizer = UITapGestureRecognizer()
    
    
    // Props
    
    var currentEventId = 603908
    
    var cellHeight = 60
    
    var currentEvent: Event?
    
    var ticketPartners: JSON?
    
        
//    var tickets = (1...6).map { $0 }
//    
//    var artists = (1...40).map { $0 }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        artistsTableView.dataSource = self
        artistsTableView.delegate = self
        
        ticketsTableView.dataSource = self
        ticketsTableView.delegate = self
        
        let url = "http://api.concertwith.me/event/\(currentEventId)"
        
        var request = NSURLRequest(URL: NSURL(string: url)!)
        
        // Качаем данные
        var data = NSURLConnection.sendSynchronousRequest(request, returningResponse: nil, error: nil)
        if data != nil {
            // Парсим json
            var json = JSON(data: data!)
            currentEvent = Event(eventJson: json["data"])
            ticketPartners = json["metadata"]["ticketsPartners"]
            
            mapTapRecognizer.addTarget(self, action: "mapTap")
            mapGradientView.userInteractionEnabled = true
            mapGradientView.addGestureRecognizer(mapTapRecognizer)
//            println(ticketPartners)
            // Заполняем вьюху данными
            fillView()
            // Пересчитываем градиенты
            recalculateImageGradient(imageView.bounds.size)
            recalculateMapGradient(mapGradientView.bounds.size)
        }
        
        
        
        
    }
    
    override func viewDidLayoutSubviews() {
        if artistsTableView.respondsToSelector(Selector("setSeparatorInset:")) {
            artistsTableView.separatorInset = UIEdgeInsetsZero
        }
        
        if artistsTableView.respondsToSelector(Selector("setLayoutMargins:")) {
            artistsTableView.separatorInset = UIEdgeInsetsZero
        }
    }
    
    func fillView() {
        
        if let event = currentEvent as Event? {
            
            imageView.setURL(NSURL(string: event.image), placeholderImage: UIImage(named: "defaultImage"))
            
            
            
            dateLabel.text = event.dateFormatted
            artistLabel.text = event.headlinerName
            
            // Площадка
            var venue = event.venue
            venueNameLabel.text = venue.name
            venueAdrLabel.text = venue.street + ", " + venue.city
            
            
            // Делаем точку
            let location = CLLocationCoordinate2D(latitude: event.lat, longitude: event.long)
            
            // Делаем зону приближения
            let span = MKCoordinateSpanMake(0.005, 0.005)
//            let span = MKCoordinateSpanMake(10, 10)
            let region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: event.lat, longitude: event.long-0.004), span: span)
            mapView.setRegion(region, animated: true)
            
            // Ставим иголку
            let annotation = MKPointAnnotation()
            annotation.coordinate = location
            annotation.title = venue.name
            mapView.addAnnotation(annotation)
            
            
            
            eventDescLabel.text = event.description
            eventDescLabel.sizeToFit()
            
            
            
            ticketsTableHeightConstraint.constant = CGFloat(event.tickets.count * cellHeight + 2)
            
            
            var tableHeight = CGFloat(event.artists.count * cellHeight + 2)
            artTableHeightConstraint.constant = tableHeight
            artistsTableView.rowHeight = CGFloat(cellHeight)
    //        artistsTableView.frame.size = CGSize(width: 100, height: tableHeight)
            
            ticketsTableView.rowHeight = CGFloat(cellHeight)

            
            
            artTopLineView.frame.size.height = 0.5
            tickTopLineView.frame.size.height = 0.5
        }
        
    }
    
    func recalculateImageGradient(size: CGSize) {
        
        
        let imageGradientLayer = CAGradientLayer()
        //        background.frame = self.view.bounds
        imageGradientLayer.frame = CGRect(x: imageView.bounds.origin.x, y: imageView.bounds.origin.y, width: size.width, height: size.height)
        imageGradientLayer.colors = [
            //            UIColor(red:0.97, green:0.96, blue:0.92, alpha:1)
            UIColor(red:0, green:0, blue:0, alpha:1).CGColor as AnyObject,
            UIColor(red:0, green:0, blue:0, alpha:0).CGColor as AnyObject,
        ]
        imageGradientLayer.startPoint = CGPoint(x: 0, y: 0)
        imageGradientLayer.endPoint = CGPoint(x: 0, y: 1)
        // Удаляем старый
        if imageView.layer.sublayers != nil  {
            imageView.layer.sublayers.removeAtIndex(0)
        }
        imageView.layer.insertSublayer(imageGradientLayer, atIndex: 0)
        
    }
    
    func recalculateMapGradient(size: CGSize) {
        let mapGradientLayer = CAGradientLayer()
        //        background.frame = self.view.bounds
        mapGradientLayer.frame = CGRect(x: mapGradientView.bounds.origin.x, y: mapGradientView.bounds.origin.y, width: size.width/2, height: size.height)
        mapGradientLayer.colors = [
            //            UIColor(red:0.97, green:0.96, blue:0.92, alpha:1)
            UIColor(red:0.97, green:0.96, blue:0.92, alpha:1).CGColor as AnyObject,
            UIColor(red:0.97, green:0.96, blue:0.92, alpha:0).CGColor as AnyObject,
        ]
        mapGradientLayer.startPoint = CGPoint(x: 0, y: 0)
        mapGradientLayer.endPoint = CGPoint(x: 1, y: 0)
//        // Удаляем старый
//        if mapGradientView.layer.sublayers != nil {
//            mapGradientView.layer.sublayers.removeAtIndex(0)
//        }
        mapGradientView.layer.insertSublayer(mapGradientLayer, atIndex: 0)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func mapView(mapView: MKMapView!, didSelectAnnotationView view: MKAnnotationView!) {
//        println("qewqweqwe")
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: UITableViewCell = UITableViewCell()
        if (tableView == artistsTableView) {
            var cell = tableView.dequeueReusableCellWithIdentifier("artCell", forIndexPath: indexPath) as! ArtistTableViewCell
            if let event = currentEvent as Event? {
                cell.photoImageView.setURL(NSURL(string: event.artists[indexPath.row].image + "?thumb=true"), placeholderImage: UIImage(named: "defaultImage"))
                cell.titleLabel.text =  String(event.artists[indexPath.row].name)
            }
            ViewHelper.normalizeTableCell(cell)
            return cell
        }
        if (tableView == ticketsTableView) {
            var cell: TicketsTableViewCell = tableView.dequeueReusableCellWithIdentifier("tickCell", forIndexPath: indexPath) as! TicketsTableViewCell
            if let event = currentEvent as Event? {
                cell.priceLabel.text =  String(event.tickets[indexPath.row].formatted)
                
                // Ставим иконку партнера
                var servId = String(event.tickets[indexPath.row].serviceId)
                var tick = ticketPartners![servId]
                cell.partnerLabel.text = tick["full_name"].string! + "(" + tick["status"].string! + ")"
                cell.partnerImageView.setURL(NSURL(string: tick["icon"].string!), placeholderImage: UIImage(named: "defaultImage"))
            }
            ViewHelper.normalizeTableCell(cell)
            return cell
        }
        ViewHelper.normalizeTableCell(cell)
        return cell
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if (tableView == artistsTableView) {
                   }
        if (tableView == ticketsTableView) {
            if let event = currentEvent as Event? {
                UIApplication.sharedApplication().openURL(NSURL(string: event.tickets[indexPath.row].link)!)
            }
        }
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if cell.respondsToSelector(Selector("setSeparatorInset:")) {
            cell.separatorInset = UIEdgeInsetsZero
        }
        
        if cell.respondsToSelector(Selector("setLayoutMargins:")) {
            cell.separatorInset = UIEdgeInsetsZero
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let event = currentEvent as Event? {
            if (tableView == artistsTableView) {
                return event.artists.count
            }
            if (tableView == ticketsTableView) {
                return event.tickets.count
            }
        } 
        return 0
    }
    
//    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
////        let  headerCell: HeaderViewCell = tableView.dequeueReusableCellWithIdentifier("headerCell") as! HeaderViewCell
////        headerCell.bounds.size.height = CGFloat(1)
////        headerCell.frame.size.height = CGFloat(1)
////        
////        return headerCell
//        var rect  = CGRect(x: 0, y: 0, width: tableView.frame.width, height: 100)
//        var headerView = UIView(frame: rect)
//        println(headerView.frame.size)
//        headerView.backgroundColor = UIColor.grayColor()
//        return headerView
//    }
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        // Пересчитываем градиенты
        recalculateImageGradient(CGSize(width: size.width, height: imageView.bounds.height))
//        recalculateMapGradient(CGSize(width: size.width, height: mapGradientView.bounds.height))
        
        if UIDevice.currentDevice().orientation.isLandscape.boolValue {
         
        } else {
            
        }
    }
    
    
    func mapTap() {
        if let event = currentEvent as Event! {
            let location = CLLocationCoordinate2D(latitude: event.lat, longitude: event.long)
            
            println("map tapped!")
            // Делаем зону приближения
            let regionDistance:CLLocationDistance = 10000
            var coordinates = CLLocationCoordinate2D(latitude: event.lat, longitude: event.long)
            let regionSpan = MKCoordinateRegionMakeWithDistance(coordinates, regionDistance, regionDistance)
            var options = [
                MKLaunchOptionsMapCenterKey: NSValue(MKCoordinate: regionSpan.center),
                MKLaunchOptionsMapSpanKey: NSValue(MKCoordinateSpan: regionSpan.span)
            ]
            var placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
            var mapItem = MKMapItem(placemark: placemark)
            mapItem.name = "\(event.venue.name)"
            mapItem.openInMapsWithLaunchOptions(options)
        }

    }
   
}
