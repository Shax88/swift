//
//  CityToEventSegue.swift
//  cwm-spring
//
//  Created by Dmitry Uskov on 10.04.15.
//  Copyright (c) 2015 Dmitry Uskov. All rights reserved.
//

import UIKit
import Spring

class CityToEventSegue: UIStoryboardSegue {
   
    override func perform() {
        // Assign the source and destination views to local variables.
        var firstVCView = self.sourceViewController.view as! DesignableView
        var secondVCView = self.destinationViewController.view as! DesignableView
//        usleep(1000000)
//        firstVCView.animation = "zoomIn"
//        firstVCView.curve = "spring"
//        firstVCView.duration = 0.3
//        firstVCView.scaleX = 10.0
//        firstVCView.scaleY = 10.0
//        firstVCView.damping = 1.0
//        firstVCView.animate()
        
        
        secondVCView.animation = "zoomIn"
        secondVCView.curve = "spring"
        secondVCView.duration = 0.6
        secondVCView.scaleX = 20.0
        secondVCView.scaleY = 20.0
        secondVCView.damping = 1
        secondVCView.animate()
        self.sourceViewController.navigationController??.pushViewController(self.destinationViewController as! UIViewController, animated: false)
        
//        // Get the screen width and height.
//        let screenWidth = UIScreen.mainScreen().bounds.size.width
//        let screenHeight = UIScreen.mainScreen().bounds.size.height
//        
//        // Specify the initial position of the destination view.
//        secondVCView.frame = CGRectMake(0.0, screenHeight, screenWidth, screenHeight)
//        
//        // Access the app's key window and insert the destination view above the current (source) one.
//        let window = UIApplication.sharedApplication().keyWindow
//        window?.insertSubview(secondVCView, aboveSubview: firstVCView)
        
        
    }
    
}
