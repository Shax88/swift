//
//  EventCardView.swift
//  cwm
//
//  Created by Dmitry Uskov on 02.04.15.
//  Copyright (c) 2015 Dmitry Uskov. All rights reserved.
//

import UIKit
import Spring

@IBDesignable class EventCardView: DesignableView {

    @IBOutlet var view: UIView!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var imageView: UIView!
    
    @IBOutlet weak var image: AsyncImageView!
    
    @IBOutlet weak var shadowView: UIView!
    
    @IBOutlet weak var roundedView: UIView!
    
    @IBOutlet weak var venueBar: UIView!
    
    @IBOutlet weak var venueLabel: UILabel!

    @IBOutlet weak var wtcButton: DesignableButton!
    
//    override var frame: CGRect {
//        get {
//            return super.frame
//        }
//        set {
//            super.frame = newValue
//            if ((view) != nil) {
//                view.frame = newValue
//            }
//        }
//    }
//   
//    override var bounds: CGRect {
//        get {
//            return super.bounds
//        }
//        set {
//            super.bounds = newValue
//            if ((view) != nil) {
//                view.bounds = newValue
//            }
//        }
//    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        xibSetup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame);
        
        xibSetup()
    }
    
    
    func xibSetup() {
        view = loadViewFromNib()
        
        // use bounds not frame or it'll be offset
        view.frame = bounds
        
        
        // Make the view stretch with containing view
        view.autoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight
        
        // Делаем тень
        var layerViewRounded = roundedView.layer
        var layerView = shadowView.layer
        
        
        //        layerViewRounded.borderColor = UIColor.grayColor().CGColor
        //        layerViewRounded.borderWidth = 0.5
        layerViewRounded.cornerRadius = 4
        layerViewRounded.masksToBounds = true
        //        roundedView.clipsToBounds = true
        
        layerView.shadowColor = UIColor.grayColor().CGColor
        layerView.shadowOffset = CGSize(width: 0, height: 2.5)
        layerView.shadowOpacity = 0.55
        layerView.shadowRadius = 5
        //        layerView.masksToBounds = false
        //        shadowView.clipsToBounds = true
        
        
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)
        
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "EventCardView", bundle: bundle)
        
        // Assumes UIView is top level and only object in CustomView.xib file
        let viewNew = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        return viewNew
    }

}
